package com.zuitt;

import java.util.Arrays;
import java.util.HashMap;

public class main {
    public static void main(String[] args) {
//     1.
        int[] primeNumbers = {2, 3, 5, 7, 11};
        System.out.println("The first prime number is: " + primeNumbers[0]);

//     2.
        String[] ArrayList = {"John", "Jane", "Chloe", "Zoey"};
        System.out.println("My friends are: " + Arrays.toString(ArrayList));

//     3.
        HashMap<String, Integer> Inventory = new HashMap<String, Integer>();

        Inventory.put("toothpaste", 15);
        Inventory.put("toothbrush", 20);
        Inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + Inventory);


    }
}